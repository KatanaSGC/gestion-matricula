﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGCollege.Models.SeccionDTO
{
    public class SeccionDTO
    {
        public int Id { get; set; }
        public string Seccion { get; set; }
    }
}
