﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SGCollege.Models
{
    public class Colaborador
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(15)]
        public string  PrimerNombre { get; set; }
        [Required]
        [MaxLength(15)]
        public string SegundoNombre { get; set; }
        [Required]
        [MaxLength(15)]
        public string PrimerApellido { get; set; }
        [Required]
        [MaxLength(15)]
        public string SegundoApellido { get; set; }
        [Required]
        public int Telefono { get; set; }
        [Required]
        [MaxLength(15)]
        public string Email { get; set; }
        [Required]
        [MaxLength(15)]
        public string Identidad { get; set; }

        public Usuarios Usuario { get; set; }
       
    }
}
