﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SGCollege.Models.EstudianteDTO
{
    public class EstudianteDTO
    {
        [Key]
        public string Identidad { get; set; }
        public string Nombre { get; set; }
        public string FechaNacimiento { get; set; }
        public string Nacionalidad { get; set; }
        public string Estado { get; set; }
    }
}
