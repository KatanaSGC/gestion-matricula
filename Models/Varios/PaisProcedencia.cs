﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SGCollege.Models.Varios
{
    public class PaisProcedencia
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nacionalidad { get; set; }
        public List<EstudianteModel> Estudiantes { get; set; }
    }
}
 