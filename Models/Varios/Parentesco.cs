﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGCollege.Models.Varios
{
    public class Parentesco
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public List<TutorModel> Tutores { get; set; }
    }
}
