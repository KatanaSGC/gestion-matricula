using System.Security.AccessControl;
using System.ComponentModel.DataAnnotations;
using SGCollege.Models;
using System.Collections.Generic;

namespace SGCollege.Models
{
    public class Seccion
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Jornada { get; set; }
        [Required]
        public int Cupos { get; set; }
        [Required]
        public string Descripcion { get; set; }
        public int CursoId { get; set; }
        public Curso Curso { get; set; }
        public List<MatriculaModel> Matriculas { get; set; }
    }
}