using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SGCollege.Models;

namespace SGCollege.Models
{
    public class EstadoModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MaxLength(25)]
        public string Descripcion { get; set; }
        public List<EstudianteModel> Estudiantes { get; set; }
    }
}