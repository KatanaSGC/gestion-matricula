using System.ComponentModel.DataAnnotations;
using SGCollege.Models;
using System.Collections.Generic;
namespace SGCollege.Models
{
    public class Curso
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string NombreCurso { get; set; }
        [Required]
        public int NivelAcademicoId { get; set; }
        [Required]
        public NivelAcademico NivelAcademico { get; set; }
    }
}