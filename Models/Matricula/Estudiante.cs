using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.AccessControl;
using SGCollege.Models;
using SGCollege.Models.Matricula;
using SGCollege.Models.Varios;

namespace SGCollege.Models {
    public class EstudianteModel 
    {
        [Key, Column (Order = 0)]
        [DatabaseGenerated (DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength (25)]
        public string PrimerNombre { get; set; }

        [MaxLength (25)]
        public string SegundoNombre { get; set; }

        [Required]
        [MaxLength (25)]
        public string PrimerApellido { get; set; }

        [MaxLength (25)]
        public string SegundoApellido { get; set; }

        [Required]
        [MaxLength (25)]
        public string Identidad { get; set; }

        [Required]
        public string FechaNacimiento { get; set; }

        [Required]
        public int? Sexo { get; set; }

        [Required]
        [MaxLength (60)]
        public string Direccion { get; set; }

        [MaxLength (15)]
        public string Telefono { get; set; }

        [Required]
        public int? EstadoId { get; set; }

        [Required]
        public int PaisProcedenciaId { get; set; }
        public int TipoSangreId { get; set; }

        public PaisProcedencia PaisProcedencia { get; set; }
        public TipoSangre TipoSangre { get; set; }
        public EstadoModel Estado { get; set; }
        public List<EstudianteTutor> EstudiantesTutores { get; set; }
        public List<MatriculaModel> Matriculas { get; set; }
    }

}