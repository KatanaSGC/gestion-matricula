using System.ComponentModel.DataAnnotations;
using SGCollege.Models;
using System.Collections.Generic;

namespace SGCollege.Models
{
    public class NivelAcademico
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}