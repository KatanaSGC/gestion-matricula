﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SGCollege.Models.Matricula
{
    public class EstudianteTutor
    {
        public int Id { get; set; }
        [Required]
        public int EstudianteId { get; set; }
        public EstudianteModel Estudiante { get; set; }
        [Required]
        public int TutorId { get; set; }
        public TutorModel Tutor { get; set; }
    }
}
