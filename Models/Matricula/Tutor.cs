using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SGCollege.Models;
using SGCollege.Models.Matricula;
using SGCollege.Models.Varios;

namespace SGCollege.Models
{
    public class TutorModel
    {
        public int Id { get; set; }
        [Required]
        [MaxLength (25)]
        public string PrimerNombre { get; set; }
        [MaxLength (25)]
        public string SegundoNombre { get; set; }
        [Required]
        [MaxLength (25)]
        public string PrimerApellido { get; set; }
        [MaxLength (25)]
        public string SegundoApellido { get; set; }
        [Required]
        [MaxLength (25)]
        public string Identidad { get; set; }
        [MaxLength (120)]
        public string DireccionDomicilio { get; set; }
        [MaxLength (60)]
        public string Profesion { get; set; }
        [MaxLength (60)]
        public string DondeTrabaja { get; set; }
        [MaxLength (15)]
        [Required]
        public string TelefonoDomicilio { get; set; }
        [MaxLength (15)]
        public string TelefonoTrabajo { get; set; }
        [MaxLength (15)]
        public string TelefonoMovil { get; set; }
        [MaxLength (120)]
        public string DireccionTrabajo { get; set; }
        [Required]
        public int ParentescoId { get; set; }
        public Parentesco Parentesco { get; set; }
        [Required]
        [MaxLength (60)]
        public string Email { get; set; }
        public List<EstudianteTutor> EstudianteTutor { get; set; }
    }
}