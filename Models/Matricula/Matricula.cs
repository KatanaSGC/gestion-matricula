using System.Security.AccessControl;
using System;
using System.ComponentModel.DataAnnotations;
using SGCollege.Models;
using System.Collections.Generic;

namespace SGCollege.Models
{
    public class MatriculaModel
    {
        public int Id { get; set; }
        [Required]
        public DateTime FechaMatricula { get; set; }
        [Required]
        public bool CertificacionEstudio { get; set; }
        [Required]
        public int PeriodoMatricula { get; set; }
        [Required]
        public int Promedio { get; set; }
        [Required]
        public bool AplicaBeca { get; set; }
        [Required]
        public int EstudianteId { get; set; }
        public EstudianteModel Estudiante { get; set; }
        public int SeccionId { get; set; }
        public Seccion Seccion { get; set; }
    }
}