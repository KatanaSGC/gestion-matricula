﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SGCollege.DataContext;
using SGCollege.Models.SeccionDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGCollege.Data
{
    public class MatriculaData
    {
        private readonly SGCollegeDB _context;

        public MatriculaData(SGCollegeDB context)
        {
            _context = context;
        }
        public async Task<ActionResult<IEnumerable<SeccionDTO>>> GetSeccionDTO()
        {
            var secciones = await _context.SeccionDTO.FromSql("EXEC SeccionDTO").ToListAsync();
            return secciones;
        }
    }
}
