﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SGCollege.DataContext;
using SGCollege.Models;
using SGCollege.Models.EstudianteDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SGCollege.Data
{
    public class EstudianteData
    {
        private readonly SGCollegeDB _context;
        public EstudianteData(SGCollegeDB context)
        {
            _context = context;
        }

        public async Task<ActionResult<IEnumerable<EstudianteDTO>>> GetEstudiantesDt()
        {
            var estudiantes = await _context.EstudianteDTO.FromSql("EXEC ListaEstudiantesDt").ToListAsync();
            return estudiantes;
        }
    }
}
