﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SGCollege.Logic;
using SGCollege.Models;
using SGCollege.Models.SeccionDTO;
using SGCollege.Services;

namespace SGCollege.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatriculaController : ControllerBase
    {
        private readonly IRepository _repository;
        private readonly MatriculaLogica _logica;

        public MatriculaController(IRepository repository, MatriculaLogica logica)
        {
            _repository = repository;
            _logica = logica;
        }

        [HttpGet("DTO")]
        public async Task<ActionResult<IEnumerable<SeccionDTO>>> GetSeccionDTO()
        {
            return await _logica.GetSeccionDTO();
        }

        [HttpPost("{Identidad}")]
        public async Task<IActionResult> PostMatricula(MatriculaModel matricula, string Identidad)
        {
            var post = await _logica.PostMatricula(matricula, Identidad);
            return StatusCode((int)post);

        }
    }
}