using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SGCollege.Services;
using SGCollege.Models;
using System.Net;
using SGCollege.Logic;
using SGCollege.Models.EstudianteDTO;
using SGCollege.Models.Varios;

namespace SGCollege.Logica
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstudianteController : ControllerBase
    {
        private readonly EstudianteLogica _logica;
        private readonly IRepository _repository;

        public EstudianteController(EstudianteLogica logica, IRepository repository)
        {
            _logica = logica;
            _repository = repository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<EstudianteModel>>> Get()
        {
            var estudiantes = await _logica.Get();
            return estudiantes;
        }

        [HttpPost]
        public async Task<IActionResult> Post(EstudianteModel estudiante)
        {
            var query = await _logica.Post(estudiante);
            return StatusCode((int)(query));
        }

        [HttpGet("DTO")]
        public async Task<ActionResult<IEnumerable<EstudianteDTO>>> GetEstudianteDTO()
        {
            return await _logica.GetEstudianteDTO();
        }

        [HttpGet("Estado")]
        public async Task<ActionResult<IEnumerable<EstadoModel>>> GetEstado()
        {
            return await _repository.GetAll<EstadoModel>();
        }

        [HttpGet("TipoSangre")]
        public async Task<ActionResult<IEnumerable<TipoSangre>>> GetTipoSangre()
        {
            return await _repository.GetAll<TipoSangre>();
        }

        [HttpGet("PaisProcedencia")]
        public async Task<ActionResult<IEnumerable<PaisProcedencia>>> GetPaisProcedencia()
        {
            return await _repository.GetAll<PaisProcedencia>();
        }

        [HttpPut]
        public async Task<IActionResult> PutAsync(EstudianteModel estudiante)
        {
            var put = await _logica.PutAsync(estudiante);
            return StatusCode((int)put);
        }
    }
}