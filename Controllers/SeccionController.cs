﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SGCollege.Logic;
using SGCollege.Services;

namespace SGCollege.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeccionController : ControllerBase
    {
        private readonly IRepository _repository;
        private readonly MatriculaLogica _logica;

        public SeccionController(IRepository repository, MatriculaLogica logica)
        {
            _repository = repository;
            _logica = logica;
        }
    }
}