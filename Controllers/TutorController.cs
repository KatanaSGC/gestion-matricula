﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SGCollege.Logic;
using SGCollege.Models;
using SGCollege.Models.Varios;
using SGCollege.Services;

namespace SGCollege.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TutorController : ControllerBase
    {
        private readonly TutorLogica _logica;
        private readonly IRepository _repository;

        public TutorController(TutorLogica logica, IRepository repository)
        {
            _logica = logica;
            _repository = repository;
        }

        [HttpPost]
        public async Task<IActionResult> PostTutor(TutorModel tutor)
        {
            var post = await _logica.PostTutor(tutor);
            return StatusCode((int)post);
        }

        [HttpGet("Parentesco")]
        public async Task<ActionResult<IEnumerable<Parentesco>>> GetParentesco()
        {
            return await _repository.GetAll<Parentesco>();
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TutorModel>>> Get()
        {
            var estudiantes = await _logica.Get();
            return estudiantes;
        }

    }
}