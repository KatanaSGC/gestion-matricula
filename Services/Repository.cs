﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

namespace SGCollege.Services
{
    public class Repository<SGCollegeDB> : IRepository where SGCollegeDB : DbContext
    {
        private readonly SGCollegeDB _context;
        public Repository(SGCollegeDB context)
        {
            _context = context;
        }

        public async Task CreatedAsync<T>(T entity) where T : class
        {
            this._context.Set<T>().Add(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync<T>(T entity) where T : class
        {
            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();
        }

        public int Count<T>(Expression<Func<T, bool>> t) where T : class
        {
            return _context.Set<T>().Count(t);
        }

        public async Task<ActionResult<IEnumerable<T>>> GetAll<T>() where T : class
        {
            return await _context.Set<T>().ToListAsync();
        }

        public T FindByIdentidad<T>(Expression<Func<T, bool>> t) where T : class
        {
            return _context.Set<T>().FirstOrDefault(t);
        }

        public async Task UpdateAsync<T>(T t, T y) where T : class
        {
            _context.Entry(y).CurrentValues.SetValues(t);
            await _context.SaveChangesAsync();
        }

        public List<T> ViewBag<T>() where T : class
        {
            return _context.Set<T>().ToList();
        }

        public T FindById<T>(Expression<Func<T, bool>> t) where T : class
        {
            return _context.Set<T>().FirstOrDefault(t);
        }
    }
}
