﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

namespace SGCollege.Services
{
    public interface IRepository
    {
        Task<ActionResult<IEnumerable<T>>> GetAll<T>() where T : class;
        Task CreatedAsync<T>(T entity) where T : class;
        Task UpdateAsync<T>(T entity, T y) where T : class;
        Task DeleteAsync<T>(T entity) where T : class;
        List<T> ViewBag<T>() where T : class;
        T FindByIdentidad<T>(Expression<Func<T, bool>> Id) where T : class;
        int Count<T>(Expression<Func<T, bool>> expression) where T : class;
        T FindById<T>(Expression<Func<T, bool>> t) where T : class;
    }
}
