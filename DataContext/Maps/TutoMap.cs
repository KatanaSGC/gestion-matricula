﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SGCollege.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGCollege.DataContext.Maps
{
    public class TutoMap : IEntityTypeConfiguration<TutorModel>
    {
        public void Configure(EntityTypeBuilder<TutorModel> builderExtensions)
        {
            builderExtensions.ToTable("Tutores", "dbo");
            builderExtensions.HasKey(q => q.Id);
            builderExtensions.Property(q => q.Id).IsRequired().UseSqlServerIdentityColumn();
            //builderExtensions.Property(q => q.Id).IsRequired().UseMySqlIdentityColumn();
            builderExtensions.Property(q => q.PrimerNombre).HasColumnType("nvarchar(25)")
                .HasMaxLength(25).IsRequired();
            builderExtensions.Property(q => q.SegundoNombre).HasColumnType("nvarchar(25)")
                .HasMaxLength(25);
            builderExtensions.Property(q => q.PrimerApellido).HasColumnType("nvarchar(25)")
                .HasMaxLength(25).IsRequired();
            builderExtensions.Property(q => q.SegundoApellido).HasColumnType("nvarchar(25)")
                .HasMaxLength(25);
            builderExtensions.Property(q => q.Identidad).HasColumnType("nvarchar(15)")
                .HasMaxLength(15);
            builderExtensions.Property(q => q.DireccionDomicilio).HasColumnType("nvarchar(60)")
                .HasMaxLength(60).IsRequired();
            builderExtensions.Property(q => q.Profesion).HasColumnType("nvarchar(60)")
                .HasMaxLength(60).IsRequired();
            builderExtensions.Property(q => q.DondeTrabaja).HasColumnType("nvarchar(60)")
                .HasMaxLength(60).IsRequired();
            builderExtensions.Property(q => q.TelefonoDomicilio).HasColumnType("nvarchar(25)")
                .HasMaxLength(25).IsRequired();
            builderExtensions.Property(q => q.TelefonoMovil).HasColumnType("nvarchar(25)")
                .HasMaxLength(25);
            builderExtensions.Property(q => q.TelefonoTrabajo).HasColumnType("nvarchar(25)")
                .HasMaxLength(25);
            builderExtensions.Property(q => q.ParentescoId).HasColumnType("int")
                .IsRequired();
            builderExtensions.Property(q => q.Email).HasColumnType("nvarchar(40)")
                .HasMaxLength(40);

            builderExtensions.HasOne(q => q.Parentesco).WithMany(q => q.Tutores).HasForeignKey(q => q.ParentescoId);
        }
    }
}
