﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SGCollege.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGCollege.DataContext.Maps
{
    public class EstudianteMap : IEntityTypeConfiguration<EstudianteModel>
    {
        public void Configure(EntityTypeBuilder<EstudianteModel> builderExtensions)
        {
            builderExtensions.ToTable("Estudiantes", "dbo");
            builderExtensions.HasKey(q => q.Id);
            builderExtensions.Property(q => q.Id).IsRequired().UseSqlServerIdentityColumn();
            //builderExtensions.Property(q => q.Id).IsRequired().UseMySqlIdentityColumn();
            builderExtensions.Property(q => q.PrimerNombre).HasColumnType("nvarchar(25)")
                .HasMaxLength(25).IsRequired();
            builderExtensions.Property(q => q.SegundoNombre).HasColumnType("nvarchar(25)")
                .HasMaxLength(25);
            builderExtensions.Property(q => q.PrimerApellido).HasColumnType("nvarchar(25)")
                .HasMaxLength(25).IsRequired();
            builderExtensions.Property(q => q.SegundoApellido).HasColumnType("nvarchar(25)")
                .HasMaxLength(25);
            builderExtensions.Property(q => q.Identidad).HasColumnType("nvarchar(15)")
                .HasMaxLength(15);
            builderExtensions.Property(q => q.FechaNacimiento).HasColumnType("nvarchar(25)")
                .HasMaxLength(25).IsRequired();
            builderExtensions.Property(q => q.Sexo).HasColumnType("int").IsRequired();
            builderExtensions.Property(q => q.Direccion).HasColumnType("nvarchar(60)")
                .HasMaxLength(60).IsRequired();
            builderExtensions.Property(q => q.Telefono).HasColumnType("nvarchar(15)")
                .HasMaxLength(15);
            builderExtensions.Property(q => q.EstadoId).HasColumnType("int").IsRequired();
            builderExtensions.Property(q => q.PaisProcedenciaId).HasColumnType("int").IsRequired();
            builderExtensions.Property(q => q.TipoSangreId).HasColumnType("int").IsRequired();

            builderExtensions.HasOne(q => q.PaisProcedencia).WithMany(e => e.Estudiantes).HasForeignKey(e => e.PaisProcedenciaId);
            builderExtensions.HasOne(q => q.Estado).WithMany(e => e.Estudiantes).HasForeignKey(e => e.EstadoId);
            builderExtensions.HasOne(q => q.TipoSangre).WithMany(e => e.Estudiantes).HasForeignKey(e => e.TipoSangreId);
        }
    }
}
