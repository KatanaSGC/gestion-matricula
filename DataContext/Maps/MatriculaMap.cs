﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SGCollege.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGCollege.DataContext.Maps
{
    public class MatriculaMap : IEntityTypeConfiguration<MatriculaModel>
    {
        public void Configure(EntityTypeBuilder<MatriculaModel> builderExtensions)
        {
            builderExtensions.ToTable("Matricula", "dbo");
            builderExtensions.HasKey(q => q.Id);
            builderExtensions.Property(q => q.Id).IsRequired().UseSqlServerIdentityColumn();
            //builderExtensions.Property(q => q.Id).IsRequired().UseMySqlIdentityColumn();
            builderExtensions.Property(q => q.FechaMatricula).HasColumnType("date")
                .IsRequired();
            builderExtensions.Property(q => q.CertificacionEstudio).HasColumnType("bit")
                .IsRequired();
            builderExtensions.Property(q => q.PeriodoMatricula).HasColumnType("int")
                .IsRequired();
            builderExtensions.Property(q => q.Promedio).HasColumnType("int")
                .IsRequired();
            builderExtensions.Property(q => q.AplicaBeca).HasColumnType("bit")
                .IsRequired();
            builderExtensions.Property(q => q.EstudianteId).HasColumnType("int")
                .IsRequired();
            builderExtensions.Property(q => q.SeccionId).HasColumnType("int")
                .IsRequired();

            builderExtensions.HasOne(q => q.Estudiante).WithMany(e => e.Matriculas).HasForeignKey(e => e.EstudianteId);
            builderExtensions.HasOne(q => q.Seccion).WithMany(e => e.Matriculas).HasForeignKey(e => e.SeccionId);
        }
    }
}
