using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SGCollege.DataContext.Maps;
using SGCollege.Models;
using SGCollege.Models.EstudianteDTO;
using SGCollege.Models.Matricula;
using SGCollege.Models.SeccionDTO;
using SGCollege.Models.Varios;
using System.ComponentModel.DataAnnotations.Schema;

namespace SGCollege.DataContext
{
    public class SGCollegeDB : IdentityDbContext<Usuarios>
    {
        public DbSet<EstudianteModel> Estudiantes { get; set; }
        public DbSet<TutorModel> Tutores { get; set; }
        public DbSet<MatriculaModel> Matriculas { get; set; }
        public DbSet<EstadoModel> Estados { get; set; }
        public DbSet<Curso> Cursos { get; set; }
        public DbSet<NivelAcademico> NivelesAcademicos { get; set; }
        public DbSet<Seccion> Secciones { get; set; }
        public DbSet<EstudianteTutor> EstudianteTutor { get; set; }
        public DbSet<TipoSangre> TipoSangre { get; set; }
        public DbSet<PaisProcedencia> PaisProcedencia { get; set; }
        public DbSet<Parentesco> Parentesco { get; set; }
        public DbSet<EstudianteDTO> EstudianteDTO { get; set; }
        public DbSet<SeccionDTO> SeccionDTO { get; set; }
        public SGCollegeDB(DbContextOptions<SGCollegeDB> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EstudianteMap());
            base.OnModelCreating(modelBuilder);
        }
    }
}