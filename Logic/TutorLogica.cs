﻿using Microsoft.AspNetCore.Mvc;
using SGCollege.Models;
using SGCollege.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

namespace SGCollege.Logic
{
    public class TutorLogica
    {
        private readonly IRepository _repository;
        public TutorLogica(IRepository repository)
        {
            _repository = repository;
        }

        public async Task<ActionResult<IEnumerable<TutorModel>>> Get()
        {
            return await _repository.GetAll<TutorModel>();
        }

        public async Task<HttpStatusCode> PostTutor(TutorModel tutor)
        {
            try
            {
                Expression<Func<TutorModel, bool>> expresion = m => m.Identidad == tutor.Identidad;
                var exist = _repository.Count<TutorModel>(expresion);
                if (exist == 0)
                {
                    await _repository.CreatedAsync<TutorModel>(tutor);
                    return HttpStatusCode.OK;
                }
                return HttpStatusCode.Found;

            }
            catch (Exception err)
            {
                Debug.WriteLine(err);
                return HttpStatusCode.BadRequest;
            }
        }
    }
}
