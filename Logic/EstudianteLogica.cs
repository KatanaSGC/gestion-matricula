﻿using Microsoft.AspNetCore.Mvc;
using SGCollege.Data;
using SGCollege.DataContext;
using SGCollege.Models;
using SGCollege.Models.EstudianteDTO;
using SGCollege.Models.Varios;
using SGCollege.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

namespace SGCollege.Logic
{
    public class EstudianteLogica
    {
        private readonly IRepository _repository;
        private readonly SGCollegeDB _dB;
        private readonly EstudianteData _estudianteData;

        public EstudianteLogica(IRepository repository, SGCollegeDB dB, EstudianteData estudianteData)
        {
            _estudianteData = estudianteData;
            _repository = repository;
            _dB = dB;
        }

        public async Task<ActionResult<IEnumerable<EstudianteDTO>>> GetEstudianteDTO()
        {
            try
            {
                return await _estudianteData.GetEstudiantesDt();
            }
            catch(Exception err)
            {
                Debug.WriteLine(err);
                return null;
            }
        }

        public async Task<HttpStatusCode> Post(EstudianteModel estudiante)
        {
            try
            {
                Expression<Func<EstudianteModel, bool>> expresion = m => m.Identidad == estudiante.Identidad;
                var exist = _repository.Count<EstudianteModel>(expresion);
                if(exist == 0)
                {
                    estudiante.FechaNacimiento = Convert.ToDateTime(estudiante.FechaNacimiento).ToShortDateString();
                    await _repository.CreatedAsync<EstudianteModel>(estudiante);
                    return HttpStatusCode.OK;
                }
                return HttpStatusCode.Created;
            }
            catch(Exception err)
            {
                Console.WriteLine(err);
                return HttpStatusCode.BadRequest;
            }
        }

        public async Task<ActionResult<IEnumerable<EstudianteModel>>> Get()
        {
            return await _repository.GetAll<EstudianteModel>();
        }

        public async Task<HttpStatusCode> PutAsync(EstudianteModel estudiante)
        {
            try
            {
                Expression<Func<EstudianteModel, bool>> count = m => m.Identidad == estudiante.Identidad;
                var existe = _repository.Count<EstudianteModel>(count);
                if (existe > 0)
                {
                    EstudianteModel student = _repository.FindById<EstudianteModel>(count);
                    estudiante.Id = student.Id;
                    await _repository.UpdateAsync<EstudianteModel>(estudiante, student);
                    return HttpStatusCode.OK;
                }
                return HttpStatusCode.Created;
            }
            catch(Exception err)
            {
                Console.WriteLine(err);
                return HttpStatusCode.BadRequest;
            }
        }
    }
}
 