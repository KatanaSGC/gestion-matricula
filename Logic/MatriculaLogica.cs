﻿using Microsoft.AspNetCore.Mvc;
using SGCollege.Data;
using SGCollege.Models;
using SGCollege.Models.SeccionDTO;
using SGCollege.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

namespace SGCollege.Logic
{
    public class MatriculaLogica
    {
        private readonly IRepository _repository;
        private readonly MatriculaData _matriculaData;

        public MatriculaLogica(IRepository repository, MatriculaData matriculaData)
        {
            _repository = repository;
            _matriculaData = matriculaData;
        }

        public async Task<HttpStatusCode> PostMatricula(MatriculaModel matricula, string Identidad)
        {
            try
            {
                Expression<Func<EstudianteModel, bool>> expressionEstudiante = m => m.Identidad == Identidad;
                EstudianteModel find = _repository.FindByIdentidad<EstudianteModel>(expressionEstudiante);

                Expression<Func<MatriculaModel, bool>> expressionMatricula = m => m.EstudianteId == find.Id;
                var count = _repository.Count<MatriculaModel>(expressionMatricula);

                if (count > 0)
                {
                    return HttpStatusCode.Found;
                }
                matricula.EstudianteId = find.Id;
                await _repository.CreatedAsync<MatriculaModel>(matricula);
                return HttpStatusCode.Created;
            }
            catch(Exception err)
            {
                Debug.WriteLine(err);
                return HttpStatusCode.BadRequest;
            }
        }

        public async Task<ActionResult<IEnumerable<SeccionDTO>>> GetSeccionDTO()
        {
            return await _matriculaData.GetSeccionDTO();
        }
    }
}
